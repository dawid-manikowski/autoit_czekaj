#include <..\..\libraries\autoit_standard_libraries\Main.au3>
#include <..\..\libraries\autoit_standard_libraries\Utils.au3>


$Main_G_GUI = False	; skrypt NIE wymaga GUI
_Main_StartScript()

$Czekaj =  _Main_GetArgument("CZEKAJ")
_Main_Logger('Odczytano argument "CZEKAJ": ' & $Czekaj)
$Czekaj = Int($Czekaj)
_Main_Logger('Int($Czekaj): ' & $Czekaj)
If $Czekaj <> 0 Then
	_Main_Logger('Rozpoczynam wstrzymaniu skryptu na: ' & $Czekaj & ' sek.')
	Sleep($Czekaj * 1000)
	_Main_Logger('Zakończono wstrzymanie skryptu')
EndIf




#Region ---=== Skrypt główny ===---
_Main_Logger('Wykonuję skrypt główny', "b")
#EndRegion ---=== Zakończenie skryptu głównego ===---


#Region ---=== Zakończenie skryptu ===---
_Main_Logger("START #Region ---=== Zakończenie skryptu ===---","b")
_Main_EndOK('Koniec skryptu: "' & @ScriptName & '"\nz czasem wstrzymania: ' & $Czekaj & 'sek.')
#EndRegion ---=== Zakończenie skryptu ===---